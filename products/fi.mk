###############################################################################
# Only use this makefile for the Google Fi devices
$(call inherit-product, vendor/partner_gms/products/gms.mk)

# GCS and Tycho apps are mandatory for Google Fi.
PRODUCT_PACKAGES += \
    Tycho \
    GCS \
    GmsConfigOverlayFi \
    sysconfig_google_fi
